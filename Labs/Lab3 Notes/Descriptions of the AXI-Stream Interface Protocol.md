

### TVALID
- Driven by the Master (output) and monitored by the Slave (input)
- Signifies that the data on the TDATA is valid 
- The master is in charge of driving (output) the TDATA bus with data that will be received (input ) by the Slave Device 


### TREADY
- Driven by the Slave (output) and monitored by the Master (input)
- Signals to the Master that the Slave is ready to accept new data when high
- If low, then the Master is expected to stall until it is ready

### TDATA
- Both the TVALID and TREADY signals must be high, signaling that there is valid data on the TDATA bus and the slave device is ready to receive it
- The slave device would then grab the data on the TDATA bus and begin to process it
- This would continue until the master device runs out of data to provide 


### TLAST
- The Master device signals the last data frame in a stream by asserted the TLAST signal along with the final TDATA value and TVALID signals on the interface
- Inform the Slave device to complete its current operation and prepare for a new data stream upon the successive upstream
- Example could be to signal the end of the operation and prepare to begin processing a new stream of related data

Tasked with creating a fixed point Qx.x mulitpy unit in a hardware design language. 

The resolution (difference between successive values) of a Qm.n or UQm.n format is always $2^{-n}$ . The range of representable values depends on the notation used

**Range of representable values in Q notations**

| Notation | Texas Instrument Notation | Arm Notation |
| :-------------------: | :-------------------: |  :-------------------: |
| Signed Qm.n | $-2^{m}$ to $+2^{m} - 2^{-n}$ | $-2^{-m-1}$ to $+2^{m-1}-2^{-n}$ |
| Unsigned UQm.n | 0 to $2^m-2^{-n}$ | 0 to $2^m-2^{-n}$


C implementation of Multiplication 
```C
// precomputed value:
#define K (1 << (Q-1))

//saturate to range of int16_t
int16_t sat16(int32_t x)
{
	if (x > 0x7FFF) return 0x7FFF;
	else if(x < -0x8000) return -0x8000;
	else return (int16_t)x;
}

int16_t q_mul(int16_t a, int16_t b)
{
	int16_t result;
	int32_t temp;

	temp = (int32_t) a * (int32_t)b ; //result type is the operand's type
	//Rounding ; mid values are rounded up
	temp += K;
	// Correct by dividing by base and saturate result 
	result = sat16(temp >> Q);

	return result;
}
```

