{
  "cells": [
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "%%shell\n",
        "# Installs the latest dev build of TVM from PyPI, with CUDA enabled. To use this,\n",
        "# you must request a Google Colab instance with a GPU by going to Runtime ->\n",
        "# Change runtime type -> Hardware accelerator -> GPU. If you wish to build from\n",
        "# source, see see https://tvm.apache.org/docs/install/from_source.html\n",
        "pip install tlcpack-nightly-cu113 --pre -f https://tlcpack.ai/wheels"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "\n",
        "\n",
        "# How to optimize convolution on GPU\n",
        "**Author**: [Haichen Shen](https://homes.cs.washington.edu/~haichen/)\n",
        "\n",
        "In this tutorial, we will demonstrate how to write a high performance\n",
        "convolution implementation in TVM. We use square size input tensors and filters\n",
        "as an example, and assume the input to convolution has a large batch. In this\n",
        "example, we use a different layout to store the data in order to achieve better\n",
        "data locality. The buffer layout is HWCN, which stands for height, width,\n",
        "channel, batch.\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## Preparation and Algorithm\n",
        "\n",
        "We use the fixed size for input tensors with 256 channels and 14 x 14\n",
        "dimensions. The batch size is 256. Convolution filters contain 512 filters\n",
        "of size 3 x 3.  We use stride size 1 and padding size 1 for the\n",
        "convolution. The following code defines the convolution algorithm in TVM.\n",
        "\n",
        "\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "import numpy as np\n",
        "import tvm\n",
        "from tvm import te\n",
        "\n",
        "# The sizes of inputs and filters\n",
        "batch = 256\n",
        "in_channel = 256\n",
        "out_channel = 512\n",
        "in_size = 14\n",
        "kernel = 3\n",
        "pad = 1\n",
        "stride = 1\n",
        "\n",
        "# Algorithm\n",
        "A = te.placeholder((in_size, in_size, in_channel, batch), name=\"A\")\n",
        "W = te.placeholder((kernel, kernel, in_channel, out_channel), name=\"W\")\n",
        "out_size = (in_size - kernel + 2 * pad) // stride + 1\n",
        "# Pad input\n",
        "Apad = te.compute(\n",
        "    (in_size + 2 * pad, in_size + 2 * pad, in_channel, batch),\n",
        "    lambda yy, xx, cc, nn: tvm.tir.if_then_else(\n",
        "        tvm.tir.all(yy >= pad, yy - pad < in_size, xx >= pad, xx - pad < in_size),\n",
        "        A[yy - pad, xx - pad, cc, nn],\n",
        "        tvm.tir.const(0.0, \"float32\"),\n",
        "    ),\n",
        "    name=\"Apad\",\n",
        ")\n",
        "# Create reduction variables\n",
        "rc = te.reduce_axis((0, in_channel), name=\"rc\")\n",
        "ry = te.reduce_axis((0, kernel), name=\"ry\")\n",
        "rx = te.reduce_axis((0, kernel), name=\"rx\")\n",
        "# Compute the convolution\n",
        "B = te.compute(\n",
        "    (out_size, out_size, out_channel, batch),\n",
        "    lambda yy, xx, ff, nn: te.sum(\n",
        "        Apad[yy * stride + ry, xx * stride + rx, rc, nn] * W[ry, rx, rc, ff], axis=[ry, rx, rc]\n",
        "    ),\n",
        "    name=\"B\",\n",
        ")"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## Memory Hierarchy\n",
        "\n",
        "We first specify the memory hierarchy for buffers. The figure below shows the\n",
        "GPU memory hierarchy. One important difference from CPU memory hierarchy is\n",
        "that GPU provides a cache buffer called shared memory, which is managed by\n",
        "programmers. Thus how to maximize the data reuse in the shared memory is\n",
        "critical to achieve high performance in GPU kernels.\n",
        "\n",
        "<img src=\"https://github.com/dmlc/web-data/raw/main/tvm/tutorial/gpu_memory_hierarchy.png\" align=\"center\" height=\"319px\" width=\"271px\">\n",
        "\n",
        "In this example, we load both Apad and W into buffer AA and WW, which are\n",
        "stored in the shared memory. These buffers will be later shared by all\n",
        "threads within the same thread block to compute the convolution. Each thread\n",
        "then loads its own part from shared buffer into their local registers, AL and\n",
        "WL. BL is a local cache of output B, which is also stored in the thread local\n",
        "registers.\n",
        "\n",
        "\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "# Designate the memory hierarchy\n",
        "s = te.create_schedule(B.op)\n",
        "s[Apad].compute_inline()  # compute Apad inline\n",
        "AA = s.cache_read(Apad, \"shared\", [B])\n",
        "WW = s.cache_read(W, \"shared\", [B])\n",
        "AL = s.cache_read(AA, \"local\", [B])\n",
        "WL = s.cache_read(WW, \"local\", [B])\n",
        "BL = s.cache_write(B, \"local\")"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## Blocking\n",
        "\n",
        "The following code splits the workload into thread blocks and individual\n",
        "threads. We follow the blocking scheme in the matrix multiply. As shown in the\n",
        "figure below, given a pixel coordinate (y, x), a thread block is responsible\n",
        "for computing a region of block_factor x block_factor (64 x 64) for output\n",
        "channels and batch. Due to the limit of shared memory space, we only load step\n",
        "x block_factor (8 x 64) data from Apad and B each time to buffers in the\n",
        "shared memory.\n",
        "\n",
        "<img src=\"https://github.com/dmlc/web-data/raw/main/tvm/tutorial/conv_gpu_blocking.png\" align=\"center\" height=\"308px\" width=\"317px\">\n",
        "\n",
        "\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "# tile consts\n",
        "tile = 8\n",
        "num_thread = 8\n",
        "block_factor = tile * num_thread\n",
        "step = 8\n",
        "vthread = 2\n",
        "\n",
        "# Get the GPU thread indices\n",
        "block_x = te.thread_axis(\"blockIdx.x\")\n",
        "block_y = te.thread_axis(\"blockIdx.y\")\n",
        "block_z = te.thread_axis(\"blockIdx.z\")\n",
        "thread_x = te.thread_axis((0, num_thread), \"threadIdx.x\")\n",
        "thread_y = te.thread_axis((0, num_thread), \"threadIdx.y\")\n",
        "thread_xz = te.thread_axis((0, vthread), \"vthread\", name=\"vx\")\n",
        "thread_yz = te.thread_axis((0, vthread), \"vthread\", name=\"vy\")\n",
        "\n",
        "# Split the workloads\n",
        "hi, wi, fi, ni = s[B].op.axis\n",
        "bz = s[B].fuse(hi, wi)\n",
        "by, fi = s[B].split(fi, factor=block_factor)\n",
        "bx, ni = s[B].split(ni, factor=block_factor)\n",
        "\n",
        "# Bind the iteration variables to GPU thread indices\n",
        "s[B].bind(bz, block_z)\n",
        "s[B].bind(by, block_y)\n",
        "s[B].bind(bx, block_x)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## Virtual Thread Split\n",
        "\n",
        "We further split the workload from a thread block to individual threads. To\n",
        "avoid *memory bank conflict*, we use virtual thread to split the area into 4\n",
        "parts, and then tile into 8x8 grids. Therefore, shown in the figure below,\n",
        "each thread computes 4 strided grids, where size of each grid is 4 x 4.\n",
        "\n",
        "<img src=\"https://github.com/dmlc/web-data/raw/main/tvm/tutorial/conv_gpu_vthread.png\" align=\"center\" height=\"188px\" width=\"268px\">\n",
        "\n",
        "\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "tyz, fi = s[B].split(fi, nparts=vthread)  # virtual thread split\n",
        "txz, ni = s[B].split(ni, nparts=vthread)  # virtual thread split\n",
        "ty, fi = s[B].split(fi, nparts=num_thread)\n",
        "tx, ni = s[B].split(ni, nparts=num_thread)\n",
        "s[B].reorder(bz, by, bx, tyz, txz, ty, tx, fi, ni)\n",
        "\n",
        "s[B].bind(tyz, thread_yz)\n",
        "s[B].bind(txz, thread_xz)\n",
        "s[B].bind(ty, thread_y)\n",
        "s[B].bind(tx, thread_x)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## Cooperative Fetching\n",
        "\n",
        "As mentioned before, each time step we need to transfer step x block_factor\n",
        "data from GPU global memory to shared memory. In order to reduce the memory\n",
        "transfer per thread, the following code lets threads in the same thread block\n",
        "coopertively fetch dependent data from global memory.\n",
        "\n",
        "\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "# Schedule BL local write\n",
        "s[BL].compute_at(s[B], tx)\n",
        "yi, xi, fi, ni = s[BL].op.axis\n",
        "ry, rx, rc = s[BL].op.reduce_axis\n",
        "rco, rci = s[BL].split(rc, factor=step)\n",
        "s[BL].reorder(rco, ry, rx, rci, fi, ni)\n",
        "\n",
        "# Attach computation to iteration variables\n",
        "s[AA].compute_at(s[BL], rx)\n",
        "s[WW].compute_at(s[BL], rx)\n",
        "s[AL].compute_at(s[BL], rci)\n",
        "s[WL].compute_at(s[BL], rci)\n",
        "\n",
        "# Schedule for A's shared memory load\n",
        "yi, xi, ci, ni = s[AA].op.axis\n",
        "ty, ci = s[AA].split(ci, nparts=num_thread)\n",
        "tx, ni = s[AA].split(ni, nparts=num_thread)\n",
        "_, ni = s[AA].split(ni, factor=4)\n",
        "s[AA].reorder(ty, tx, yi, xi, ci, ni)\n",
        "s[AA].bind(ty, thread_y)\n",
        "s[AA].bind(tx, thread_x)\n",
        "s[AA].vectorize(ni)  # vectorize memory load\n",
        "\n",
        "# Schedule for W's shared memory load\n",
        "yi, xi, ci, fi = s[WW].op.axis\n",
        "ty, ci = s[WW].split(ci, nparts=num_thread)\n",
        "tx, fi = s[WW].split(fi, nparts=num_thread)\n",
        "_, fi = s[WW].split(fi, factor=4)\n",
        "s[WW].reorder(ty, tx, yi, xi, ci, fi)\n",
        "s[WW].bind(ty, thread_y)\n",
        "s[WW].bind(tx, thread_x)\n",
        "s[WW].vectorize(fi)  # vectorize memory load"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## Generate CUDA Kernel\n",
        "\n",
        "Finally we use TVM to generate and compile the CUDA kernel, and evaluate the\n",
        "latency of convolution.\n",
        "\n",
        "\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "func = tvm.build(s, [A, W, B], \"cuda\")\n",
        "dev = tvm.cuda(0)\n",
        "a_np = np.random.uniform(size=(in_size, in_size, in_channel, batch)).astype(A.dtype)\n",
        "w_np = np.random.uniform(size=(kernel, kernel, in_channel, out_channel)).astype(W.dtype)\n",
        "a = tvm.nd.array(a_np, dev)\n",
        "w = tvm.nd.array(w_np, dev)\n",
        "b = tvm.nd.array(np.zeros((out_size, out_size, out_channel, batch), dtype=B.dtype), dev)\n",
        "func(a, w, b)\n",
        "evaluator = func.time_evaluator(func.entry_name, dev, number=1)\n",
        "print(\"Convolution: %f ms\" % (evaluator(a, w, b).mean * 1e3))"
      ]
    }
  ],
  "metadata": {
    "kernelspec": {
      "display_name": "Python 3",
      "language": "python",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.8.16"
    }
  },
  "nbformat": 4,
  "nbformat_minor": 0
}
