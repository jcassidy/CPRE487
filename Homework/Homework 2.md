
1. Revisit the application of deep learning you found in the previous homework. What are  
the likely metrics that matter the most for this application? Why? Be thorough. Consider  
at least three broad types of metric (e.g., performance, energy/power, $ cost) and provide  
specifics (e.g., specific accuracy metric of interest) with references. If you cannot find these,  
make a reasonable guess and justification.

The most important metric that I found for this is performance for  prediction accuracy because the article that I looked up was about a DNN to predict in-hospital mortality, need for mechanical ventilation, and long length of stay for COVID-19 patents. Through out the article, they talked about how important how the model consistently achieved high prediction accuracy. Also explained that the accuracy of the model could help clinicians to predict future outcomes on the first day of hospital encounters, in those scenarios it predicted in-hospital mortality with specificity of 70 * 93% at 95%. The writers of this article said that predicting the probability of adverse events should be informative enough for clinicians to make appropriate decisions on admission and might not be limited to a specific time range. 


2. Calculate the number of operations in the layer

CONV layer - ((shape of width of the filter * shape of height of the filter * number of filters in the previous layer) * number of filters )

FC layer - ((current layer neurons c * previous layer neurons p) * c)


Layer 1 = 3 x 3 x 256 x 256

Layer 2 = 1 x 7 x 64 x 64

Layer 3 = 7 x 1 x 64 x 16

Layer 4 = 16 x 16
